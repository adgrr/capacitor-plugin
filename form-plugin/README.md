# Plugin Capacitor Android & iOS
Pour ce projet je me suis dirigé vers l'exercice 1: Création d'un formulaire simple.

1. [ ] Pour démarrer la procédure, dans la PWA, l'utilisateur clique sur un bouton ;
2. [ ] Lorsque l'utilisateur clique sur un bouton, le PWA doit lancer l'exécution du plugin ;
3. [ ] Le plugin doit afficher une vue native avec un formulaire pour recueillir les entrées de l'utilisateur.
Le formulaire doit contenir :
    - 1 champ de texte court (ex. demander le nom de l'utilisateur) ;
    - 1 champ de texte long (ex. demander à l'utilisateur une longue description) ;
    - 1 question à choix multiple (ex. demander à l'utilisateur de choisir plusieurs options) ;
    - 1 question à choix unique (ex. demander à l'utilisateur de choisir une seule réponse) ;
    - 1 bouton d'envoi.
4. [ ] Lorsque l'utilisateur clique sur le bouton d'envoi, le plugin doit fermer le formulaire natif et envoyer les réponses de l'utilisateur à la PWA.
5. [ ] La PWA doit être capable de récupérer les données du formulaire natif (par exemple, pour les afficher ou faire autre chose avec elles).

# Comportement
Lors du clic sur le bouton envoyer depuis la vue native (iOS & Android), les informations du formulaire sont passées à la PWA.
Puis utiliser ces données en les affichant par exemple.


# Mes étapes majeures:
Je suis parti du travail d'introduction à Ionic - Angular trouvable ici: https://ionicframework.com/docs/angular/your-first-app
Sur ce projet j'ai ajouté, comme demandé un bouton <ion-button> qui ne fait rien pour le moment.
J'ai également ajouté l'intégration iOS. 

J'ai ensuite crée le plugin capacitor : ```npx @capacitor/cli plugin:generate``` présent dans ce git.

J'ai ajouté ce plugin au projet en faisant un link (https://capacitorjs.com/docs/plugins/workflow)

```npm link``` depuis le plugin

```npm link form-plugin``` ET ```npm install form-plugin``` depuis la PWA

Dans mon cas cela n'a pas fonctionné j'ai dû uniquement effectué cette ligne depuis le projet afin de lui lier le plugin :

```npm install ../PluginTestApp```

## Developpement du plugin
Liens: 
Tutos de Simon Grimm
- https://www.youtube.com/watch?v=Nf-mOfmD7X4
- https://devdactic.com/build-capacitor-plugin/

2 méthodes : 
- showForm() : Qui affiche le page de formulaire en vue native Jetpack Compose attribuer au bouton (côté Framework)
- sendValues() : Lors du clic sur le bouton du formulaire afin de renvoyer les données sous forme de tableau et fermer la page native (côté natif)

**Partie Android**

Ouvrir le dossier android avec Android Studio puis ouvrir le fichier depuis java/com/crea/FormPlugin.java

- J'ai fait clic droit et convert to Kotlin afin de le convertir automatiquement en Kotlin

==> GROS BUG Kotlin tout souligné en rouge


On récupère ensuite les donnés grâce depuis le fichier .ts (Dans lequel on importe le plugin)
Et on affiche avec un *ngFor côté html afin de boucler et afficher les données du tableau.


**Partie iOS**

Ouvrir le fichier Plugin.xcworkspace avec Xcode 12.

Conversion en SwiftUI ?



**Formulaire en SwiftUI** : 
Liens : 
- https://www.simpleswiftguide.com/swiftui-form-tutorial-how-to-create-and-use-form-in-swiftui/
- https://stackoverflow.com/questions/58705155/what-is-the-equivalent-of-textarea-in-swiftui
- https://www.simpleswiftguide.com/swiftui-form-tutorial-how-to-create-and-use-form-in-swiftui/

@State pour garder les informations des différents champs

À savoir que :
- Textfield = Champ texte 
- TextEditor = Camp texte long (textarea)
- Picker = Liste déroulante 1 choix
- ? MultiSelector = Choix multiple











