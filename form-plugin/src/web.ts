import { WebPlugin } from '@capacitor/core';
import { FormPluginPlugin } from './definitions';

export class FormPluginWeb extends WebPlugin implements FormPluginPlugin {
  constructor() {
    super({
      name: 'FormPlugin',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  showForm(): Promise<any> {
    console.log("Formulaire activé")
    return Promise.resolve('Bien joué !');
  }
}

const FormPlugin = new FormPluginWeb();

export { FormPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(FormPlugin);
